﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerObserver : MonoBehaviour
{
    public Text countText;
    public Text winText;

    public GameObject spottedPrefab;
    public Transform spawnPosition;
    
    public GameObject player;
    public GameObject critter;
    public float Distance_;

    public GameObject critterSpawner;
    CritterSpawner critterSpawnerScript;

    public AudioSource critterFound;

    AudioSource critterAudio;

    
    
    
    private int count;
    // Start is called before the first frame update
    void Start()
    {
        GameController.gc.player = this;
        critterSpawnerScript = critterSpawner.GetComponent<CritterSpawner>();
        count = 0;
        SetCountText();
        winText.text = "";
        critterAudio = critter.GetComponent<AudioSource>();
        
    }


    private void Update()
    {
        

        //Looping through each critter in the critterPool Lost
        foreach (Critter critter in GameController.gc.critterPool)
        {
            Distance_ = Vector3.Distance(player.transform.position, critter.transform.position);

            //comparing direction vectors to ourselves to create a direction
            Vector3 direction = critter.transform.position - transform.position;
            direction = new Vector3(direction.x, 0, direction.z);
            direction = direction.normalized;
            //turning the direction into a dot
            Vector3 facing = transform.forward;
            facing = new Vector3(facing.x, 0, facing.z);
            facing = facing.normalized;
            float dot = Vector3.Dot(facing, direction);
            
            //checks to see if the player is viewing the critter
            if (dot > 0.25 && Distance_ <= 2)
            {
                GameObject excalamation = Instantiate(spottedPrefab, spawnPosition.transform.position, spottedPrefab.transform.rotation);
                print("I see you" + dot);
                
            }
            //if player is not viewing the critter, debug print to see if it is not visible
            else
            {
          
                print("I don't see you");
                
                
            }
            if (Input.GetKeyUp(KeyCode.Space) && Distance_ <= 2 && dot >= 0.25)
            {
                Destroy(critter);
                critterSpawnerScript.critterCount -= 1;
                count = count + 1;
                SetCountText();
                critterFound.Play();
                critterAudio.Stop();
            }
        }
        
        

        
    }
    void SetCountText()
    {
        countText.text = "Animals Found: " + count.ToString();
        if (count >= 10)
        {
            winText.text = "You Found Them!";
        }
    }
}
