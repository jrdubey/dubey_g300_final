﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gc;
    public PlayerObserver player;
    
    public int score;

    public List<Critter> critterPool;

    // Start is called before the first frame update
    void Awake()
    {
        if (gc == null)
        {
            gc = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

        critterPool = new List<Critter>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
