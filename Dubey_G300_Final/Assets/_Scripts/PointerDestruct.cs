﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerDestruct : MonoBehaviour
{
    public float timeTilDestroy = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, timeTilDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
