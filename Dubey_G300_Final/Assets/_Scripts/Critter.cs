﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Critter : MonoBehaviour
{
    public Critter critter;
    

    // Start is called before the first frame update
    void Start()
    {
        GameController.gc.critterPool.Add(this);   
    }

    private void OnDestroy()
    {
        GameController.gc.critterPool.Remove(this);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
