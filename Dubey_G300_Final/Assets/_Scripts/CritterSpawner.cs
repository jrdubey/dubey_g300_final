﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CritterSpawner : MonoBehaviour
{
    public GameObject critter;
    public int xPos;
    public int zPos;
    public int critterCount;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CritterSpawn());
    }

    IEnumerator CritterSpawn()
    {
        while (critterCount < 9)
        {
            xPos = Random.Range(-1, 20);
            zPos = Random.Range(-11, 16);
            Instantiate(critter, new Vector3(xPos, 2.25f, zPos), Quaternion.identity);
            yield return new WaitForSeconds(1);
            critterCount += 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
